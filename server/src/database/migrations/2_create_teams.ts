import Knex from 'knex';

export async function up(knex: Knex) {
    return await knex.schema.createTable('teams', function(table){
        table.increments('id');
        table.string('name').notNullable();
        table.integer('points').defaultTo(0);
        table.json('members').notNullable();
        table.json('goals');
        table.string('repos');
    });
};

export async function down(knex: Knex){
    return await knex.schema.dropTable('teams');
};
