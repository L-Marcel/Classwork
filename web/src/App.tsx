import React from 'react';
import Routes from './routes';

import './css/global.css';
import './css/commun.css';
import './css/header.css';
import './css/login.css';
import './css/shadow.css';
import './css/scrollbar.css';
import './css/box-list.css';
import './css/recharts.css';
import './css/directory.css';
import './css/readme.css';
import './css/dropzone.css';
import './css/classes.css';

const App = () => {
  return (
    <div>
      <Routes/>
    </div>
  );
}

export default App;
