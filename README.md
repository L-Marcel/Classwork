# Classwork
![alt text](https://img.shields.io/github/commit-activity/w/L-Marcel/Classwork?style=flat-square)
![alt text](https://img.shields.io/github/last-commit/L-Marcel/Classwork?color=informational&style=flat-square)
![alt text](https://img.shields.io/github/license/L-Marcel/Classwork?color=informational&style=flat-square)
![alt text](https://img.shields.io/discord/715640501126037579?color=informational&label=discord&style=flat-square)
![alt text](https://img.shields.io/github/languages/count/L-Marcel/Classwork?style=flat-square)

Sistema que permite ao usuário acessar todos os seus repositórios, criar ou participar de turma, entrar em equipes de uma determinada
turma e cumprir as metas da sua equipe dadas pelo professor. A ideia se baseia em uma relação de professor para aluno, onde o responsável 
pela turma tem acesso direto as informações do repositório da equipe, que é gerênciado por um de seus membros.

## Qual o objetivo?
O objetivo do Classwork é gerar um **relatório do desempenho de cada membro da equipe** que pode ser visualizado pelo professor (ou de um respoitório próprio do usuário que poderá ser acessado somente por ele)

## Status do projeto
Em desenvolvimento...

## Metas
- Geral
  - [x] Criar repositório do projeto no Github;
  - [x] Criar readme do repositório;
  - [X] Atualizar estrutura do Backend para suportar o Typescript;
  - [x] Atualizar estrutura do Frontend para suportar o Typescript;
  - [x] Webhook com o discord;
  - [x] Configurar para deploy.
  
- Backend
  - [x] Implementar a API do Github;
  - [x] Criar entidades;
  - [x] Implementar importação de arquivos;
  - [x] Implementar gerênciamento básico das informações do usuário;
  - [x] Implementar gerênciamento básico das informações das turmas;
  - [ ] Implementar gerênciamento básico das informações das equipes.
  
- Frontend
  - [x] Página de login;
  - [x] Menu lateral expansivo;
  - [x] Página de ajuda, sucesso e erro;
  - [x] Listagem de repositórios publicos;
  - [x] Relatório do repositório;
  - [x] Relatório de um commit especifico;
  - [x] Implementar graficos;
  - [x] Leitura de arquivos do diretório;
  - [x] Mostrar readme formatado;
  - [x] Página de listagem de turmas;
  - [x] Página de criação de turmas;
  - [ ] Página da turma;
  - [ ] Página de listagem de equipes;
  - [ ] Página de criação de equipes;
  - [ ] Página da equipe.

- Discord
  - Aguardando conclusão das funcionalidades do Backend e Frontend.
  
- Mobile 
  - Aguardando conclusão das funcionalidades do Backend e Frontend.

## Informações adicionais
A aplicação só funcionará com um arquivo .env que armazene as seguintes variáveis:
```
REACT_APP_GH_BASIC_CLIENT_ID=[ID DO CLIENT DA SUA APLICAÇÂO NO GITHUB]
REACT_APP_GH_BASIC_SECRET_ID=[ID SECRETO DA SUA APLICAÇÃO NO GITHUB]
REACT_APP_GH_BASIC_CLIENT_ID_DEV=[ID DO CLIENT DA SUA APLICAÇÂO LOCAL NO GITHUB]
REACT_APP_GH_BASIC_SECRET_ID_DEV=[ID SECRETO DA SUA APLICAÇÃO LOCAL NO GITHUB]
REACT_APP_STATE=[ESCOLHA PESSOAL]
REACT_APP_URL_BACK=http://localhost:3333
REACT_APP_URL_FRONT=http://localhost:3000
REACT_APP_PORT_BACK=3333
REACT_APP_PORT_FRONT=3000
REACT_APP_DB_IDENTITY=[RECOMENDADO]
```
Obs: Tanto a pasta Frontend quanto a Backend deve possuir esse arquivo.

## Problemas
- Atualmente estou tendo problemas de lentidão na leitura de repositórios com mais de 30 commits, isso é normal, entretanto, pode prejudicar a experiência do usuário. Uma das soluções possiveis é limitar a quantidade de commits carregados no relatório.

## Conteúdo em destaque

<div class="row">
  <img src="https://github.com/L-Marcel/Classwork/raw/master/web/public/images/Login.gif" width="48%" alt="Logo"/>
  <img src="https://github.com/L-Marcel/Classwork/raw/master/web/public/images/Load.gif" width="48%" alt="Load"/>
</div>
<img src="https://github.com/L-Marcel/Classwork/raw/master/web/public/images/Relatório.gif" width="100% alt="Relatório"/>



