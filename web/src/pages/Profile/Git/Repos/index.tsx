import React, { useState, useEffect, ChangeEvent } from 'react';
import { useHistory, useLocation } from "react-router-dom";

import HeaderAuth from '../../../../components/headerAuth';
import IconsLanguage from '../../../../components/iconsLanguage';

import axios from 'axios';
import api from '../../../../services/api';

import checkIfIsAuthenticated from '../../../../utils/checkIfIsAuthenticated';
import returnNotNullSession from '../../../../utils/returnNotNullSession';
import { Repos, ActionInterface } from '../../../../interfaces/global';
import { isString, isNullOrUndefined } from 'util';
import { FaSync, FaAngleRight, FaAngleLeft, FaAngleDoubleLeft, FaAngleDoubleRight } from 'react-icons/fa';


const ReposPublic = () =>{
    const history = useHistory();
    const location = useLocation();

    const [page, setPage] = useState(1);
    const [query, setQuery] = useState('');
    const [reposGitHubInPage, setReposGitHubInPage] = useState<Repos[]>([]);
    const [reposGitHub, setReposGitHub] = useState<Repos[]>([]);
    const [searching, setSearching] = useState(false);
    const [loading, setLoading] = useState(false);
    const [commitsMax, setCommitsMax] = useState("Carregando...");
    const [loadCount, setLoadCount] = useState(0);
    const [commitsLimit, setCommitsLimit] = useState(localStorage.getItem('defaultCommitsLimit')? Number(localStorage.getItem('defaultCommitsLimit')):30);
    const [commitsLimitIndex, setCommitsLimitIndex] = useState(2);
    const reposPerPage = 33;
    const reposPageMax = Math.ceil(reposGitHub.length/reposPerPage );
    const reposPageMin = 1;

    const buttonLimitValues = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1];
    const timeCommitsPreview = [7, 8, 8, 14, 15, 15, 21, 22, 22, 30, "--"];

    checkIfIsAuthenticated(sessionStorage, history, location);

    useEffect(() => {
        async function getReposGitHub(){
            var url = JSON.parse(returnNotNullSession('user', '')).urls[1];
            var token = returnNotNullSession('token', '');
            console.log(url, 'a')
    
            var pages = [] as any[];
            await api.get(process.env.REACT_APP_URL_BACK + `/git/user/repos?token=${token}&page=${1}&url=${url}`)
            .then(async function(res){
                pages.push(res.data);
                for(var j = 0; j < pages.length; j++){
                    if(pages[j].length === reposPerPage){
                        var _continuosData = await axios.get(process.env.REACT_APP_URL_BACK + `/git/user/repos?token=${token}&page=${Number(j) + 2}&url=${url}`);
                        pages[Number(j) + 1] = _continuosData.data;
                    }
                }
    
                var result = [] as any[];
                for(var k in pages){
                    for(var u in pages[k]){
                        result.push(pages[k][u]);
                    }
                }
                
                setReposGitHub(result);
                setSearching(false);
            }).catch(function(err){
                history.push('/error');
            });
        }

        setSearching(true);
        getReposGitHub();
    }, [history]);

    useEffect(() => {
        var result = [];
        var startIndex = reposPerPage * (page - 1);
        var endIndex = (reposPerPage * (page));
        for(var i = startIndex; i < endIndex; i++){
            if(!isNullOrUndefined(reposGitHub[i])){
                result.push(reposGitHub[i]);
            }
        }
        setReposGitHubInPage(result);
    }, [page, reposGitHub]);

    useEffect(() => {
        var result = [];
        var count = 0;
        for(var i in reposGitHub){
            if(reposGitHub[i].name.toLowerCase().includes(query.toLowerCase())){
                if(count < reposPerPage){
                    result.push(reposGitHub[i]);
                    count++;
                }else{
                    break;
                }
            }
        }

        setReposGitHubInPage(result);
    }, [query, reposGitHub]);

    var reposListGitHub = (reposGitHubInPage && !isString(reposGitHubInPage) && reposGitHubInPage.length > 0) && reposGitHubInPage.map(function(item) {
        var namePart = item.name.split('-');

        var description = item.description;
        var name = '';

        for(var i in namePart){
            if(Number(i) >= namePart.length - 1){
                name += namePart[i];
            }else{
                name += namePart[i] + ' ';
            }
        }

        if(name.length > 24){
            name = name.substr(0, 24) + '...';
        }

        if(description === null || description === ""){
            description = "Sem descrição";
        }else if(description.length >= 128){
            description = description.substr(0, 125) + "...";
        }

        var actionName = 'actions@'+ item.commits_url.split('/')[5] +`${commitsLimit !== -1? ('#' + commitsLimit):('')}`;
        var haveActionSave = sessionStorage.getItem(actionName)? true:false;

        return (<li key={item.id}><button className={(sessionStorage.getItem('token') != null)? "shadow-theme-lit":"shadow-theme-lit-red"} 
            style={{ color: (sessionStorage.getItem('token') != null)? 'steelblue':'#b44646', 
                paddingBottom: haveActionSave? 0:20
            }} onClick={() => {
            if(sessionStorage.getItem('token') != null){
                getCommits(item.commits_url, name, item.description);
            }}
        }>
                <div className="repos-div-space">
                    <div>
                        <h3 className={(sessionStorage.getItem('token') != null)? "repos-name":"repos-name-red"}>{name}</h3>
                        <h4 className={(sessionStorage.getItem('token') != null)? "repos-description":"repos-description-red"}>{description}</h4>
                    </div>
                    <div className="repos-div-language">
                        <IconsLanguage name={item.language} size={25}/>
                        <h5 className="repos-language">{item.language}</h5>
                    </div>
                </div>
                {haveActionSave && (<div className="repos-button-reload" onClick={(e) => {handleResetAction(
                    e, actionName, item.commits_url, name, item.description
                )}}><FaSync/>Atualizar dados e abrir</div>)}
            </button>
        </li>);
    })

    function handleChangeLimit(e: React.MouseEvent<HTMLButtonElement, MouseEvent>){
        localStorage.setItem('defaultCommitsLimit', e.currentTarget.value);
        setCommitsLimit(Number(e.currentTarget.value));
        setCommitsLimitIndex(Number(e.currentTarget.name));
    }

    function handleResetAction(e: React.MouseEvent<HTMLDivElement, MouseEvent>, 
        actionName: string, url: string, actionTitle: string, actionDescription: string){
        e.preventDefault();
        e.stopPropagation();
        sessionStorage.removeItem(actionName);
        getCommits(url, actionTitle, actionDescription);
    }

    function handleChangeQuery(e: ChangeEvent<HTMLInputElement>){
        console.log(e.currentTarget.value);
        if(isNullOrUndefined(e.currentTarget.value)){
            e.currentTarget.value = '';
        }
        console.log(e.currentTarget.value);
        setQuery(e.currentTarget.value);
    }

    function handleChangePage(e: ChangeEvent<HTMLInputElement>){
        setPage(Number(e.target.value));
    }

    function goToPage(num: number, func: string){
        if((num <= reposPageMax && func === "next") || (num >= reposPageMin && func === "back")){
            setPage(num);
        }
    }

    async function getCommits(url: string, actionTitle: string, actionDescription: string){
        setLoading(true);
        var _loadCount = loadCount + 1; 
        setLoadCount(loadCount + 1);
        const REPOS_NAME = url.split('/')[5];
        var ACTION_CHECK = sessionStorage.getItem('actions@'+REPOS_NAME+`${commitsLimit !== -1? ('#' + commitsLimit):('')}`);
        if(ACTION_CHECK == null){
            const TOKEN = sessionStorage.getItem('token');

            const ACTIONS: ActionInterface = {
                title: actionTitle,
                description: actionDescription,
                shas: [],
                commits: [],
                rank: [],
            };

            url = url.replace('{/sha}','');

            var _limitLoad = commitsLimit !== -1? commitsLimit:100;

            await axios.get(url + `?per_page=${_limitLoad}`, {
                headers: {
                    'Authorization': 'token ' + TOKEN
                }
            }).then(async function(res){
                var pages = [res.data];
                for(var j in pages){
                    if(commitsLimit === -1 && pages[j].length === _limitLoad){
                        var _continuosData = await axios.get(url + `?per_page=30&page=${Number(j)+2}`, {
                            headers: {
                                'Authorization': 'token ' + TOKEN
                            }
                        });
                        pages[Number(j) + 1] = _continuosData.data;
                    }
                }

                for(var k in pages){
                    if(Number(k) !== 0){
                        for(var u in pages[k]){
                            res.data.push(pages[k][u]);
                        }
                    }
                }

                var rank_void = true;
                var rank_index = 0;
                setCommitsMax(res.data.length);
                for(var z in res.data){
                    ACTIONS.shas[Number(z)] = res.data[z].sha;
                    
                    const com = await axios.get(url + '/' + ACTIONS.shas[Number(z)], {
                        headers: {
                            'Authorization': 'token ' + TOKEN
                        }
                    })
                    
                    var FILES: any = Object.values(com.data)[10];
                    
                    for(var x in FILES){
                        FILES[x] = {
                            filename: FILES[x].filename,
                            status:  FILES[x].status,
                            additions: FILES[x].additions,
                            deletions: FILES[x].deletions,
                            raw_url: FILES[x].raw_url,
                            changes: FILES[x].changes
                        }
                    }

                    let _userData: any = Object.values(com.data)[6];
                    let _authorData: any = Object.values(com.data)[2];
                    let _changeData: any = Object.values(com.data)[9];

                    let authorName: string = _userData.login;
                    let authorAvatar: string = _userData.avatar_url;
                    ACTIONS.commits[Number(z)] = {
                        author: authorName,
                        author_avatar: authorAvatar,
                        date: _authorData.author.date,
                        stats: _changeData,
                        message: _authorData.message,
                        tree:  _authorData.tree.url,
                        files: FILES
                    }
                    

                    if(!rank_void){
                        var _authors = [] as any[];
                        for(var n in ACTIONS.rank){
                            _authors[n] = ACTIONS.rank[n].name;
                        }

                        if(_authors.includes(_userData.login)){
                            let a = _authors.indexOf(_userData.login);
                            ACTIONS.rank[a].total += _changeData.total;
                            ACTIONS.rank[a].additions += _changeData.additions;
                            ACTIONS.rank[a].deletions += _changeData.deletions;
                        }else if(!authorName.includes('[bot]')){
                            ACTIONS.rank[rank_index] = { 
                                name: authorName,
                                avatar: authorAvatar,
                                total: _changeData.total,
                                additions: _changeData.additions,
                                deletions: _changeData.deletions,
                            };
                            rank_index++;
                        }
                    }else if(!authorName.includes('[bot]')){
                        rank_void = false;
                        ACTIONS.rank[rank_index] = { 
                            name: authorName,
                            avatar: authorAvatar,
                            total: _changeData.total,
                            additions: _changeData.additions,
                            deletions: _changeData.deletions,
                        };

                        rank_index++;
                    }
                }

                sessionStorage.setItem('actions@'+REPOS_NAME+`${commitsLimit !== -1? ('#' + commitsLimit):('')}`, JSON.stringify(ACTIONS));
            }).catch(function(){
                history.push('/error');
            });
        }
    

        if(location.pathname !== "/error"){
            sessionStorage.setItem('action', REPOS_NAME+`${commitsLimit !== -1? ('#' + commitsLimit):('')}`);
            setLoading(false);
            if(loadCount === _loadCount - 1){
                history.push('/profile/git/repos/commits');
            }
        }else{
            setLoading(false);
        }
    }

    return(
        <div>
            <HeaderAuth title='Repositório'/>
            <div className="div-global-page with-scroll header-is-auth">
                <div className="div-introduction-title-normal">
                        <h1>Repositórios públicos.</h1>
                        <h2>Lista de todos os seus repositórios públicos do Github:</h2>
                </div>
                {(sessionStorage.getItem('token') != null)? (<><div className="commits-limit-session">
                    <h1>Commits</h1>
                    {buttonLimitValues.map(function(item: number, index: number){
                        return(<button key={item}
                            className={item === commitsLimit? "commits-limit-session-button-selected":""}
                            value={item}
                            name={String(index)}
                            onClick={handleChangeLimit}
                        >
                            {item !== -1? item:"100+"}
                        </button>)
                    })}
                    <h1>Resultado</h1>
                    <p>{commitsLimit !== -1? `Carregar os ${commitsLimit} últimos commits`:"Carregar todos os commits"}</p>
                    <h1>Esperado</h1>
                    <p>{commitsLimitIndex !== 10? `${timeCommitsPreview[commitsLimitIndex]} segundos`: timeCommitsPreview[10]}</p>
                </div>
                <div className="commits-limit-session commits-limit-session-query">
                    <h1>Pesquisar</h1>
                    <input value={query} onChange={handleChangeQuery}></input>
                    <button onClick={() => {
                        setQuery('');
                    }}>Limpar</button>
                    <h1>Página</h1><div className="table-page-control-div-repos-query">
                        <button onClick={() => {goToPage(reposPageMin, "back")}} style={{color: (page > reposPageMin)? "white":"rgb(148, 182, 211)"}}><FaAngleDoubleLeft size={18}/></button>
                        <button onClick={() => {goToPage(page - 1, "back")}} style={{color: (page > reposPageMin)? "white":"rgb(148, 182, 211)"}}><FaAngleLeft size={18}/></button>
                            
                        <input min={reposPageMin} max={reposPageMax} type="number" value={page} onChange={handleChangePage}></input>
                    
                        <button onClick={() => {goToPage(page + 1, "next")}} style={{color: (page < reposPageMax)? "white":"rgb(148, 182, 211)"}}><FaAngleRight size={18}/></button>
                        <button onClick={() => {goToPage(reposPageMax, "next")}} style={{color: (page < reposPageMax)? "white":"rgb(148, 182, 211)"}}><FaAngleDoubleRight size={18}/></button>
                    </div>
                </div>
                </>):(<div className="warning-session-title">
                    <h1>Essa sessão só está disponível com a autenticação do Github</h1>
                </div>)}
                <div className={(sessionStorage.getItem('token') != null)? "container-box-list":"container-box-list container-box-list-red"}>
                    <ul>
                        {(reposGitHub && !isString(reposGitHub) && reposGitHub.length > 0)? reposListGitHub:(searching)? 
                        <div className="warning-session-title" style={{ backgroundColor: 'steelblue'}}>
                        <h1>Buscando repositórios...</h1>
                        </div>:<div className="warning-session-title" style={{ backgroundColor: 'steelblue'}}>
                        <h1>Você não possui nenhum repositório</h1>
                        </div>}
                    </ul>
                </div>
            </div>
            {loading? (<><div className="box-alert load-bar">
                    <h3>Carregando dados... Por favor, aguarde. Esse evento pode demorar conforme a quantidade de commits!</h3>
                    <div>
                        <div className="load-bar-div">
                            <div>
                                <FaSync size={18} className="rotate" color="white"/>
                                <h2>{`Commits: ${commitsMax}`}</h2>
                            </div>
                        </div>
                        <div className="load-bar-div-deco"/>
                    </div>
            </div><div className="load-bar-background"></div></>):null}
        </div>
    );
}

export default ReposPublic;