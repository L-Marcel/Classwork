import User from './UserObject';
import Class from './ClassObject';
import Git from './GitObject';

class ExternalSecurity {
    user = new User();
    class = new Class();
    git = new Git();
}

export default ExternalSecurity;