export const NotNullUser = {
    git_id: -1,
    id_auth: "",
    email: "Indefinido",
    password: "Indefinido",
}

export interface UserObject {
    git_id: number;
    id_auth: string;
    email: string;
    password: string;
}

export interface UserCredentials {
    email: string;
    password: string;
}

export interface Repos {
    commits_url: string;
    description: string;
    id: number;
    language: string;
    name: string;
    private: boolean;
    size: number;
}

export interface ActionInterface {
    title: string;
    description: string;
    shas: string[];
    commits: CommitInterface[];
    rank: {
        name: string;
        total: number;
        additions: number;
        deletions: number;
        avatar: string;
    }[];
}

export interface CommitInterface {
    author: string;
    author_avatar: string;
    date: string;
    stats: {
        total: number;
    };
    message: string;
    tree:  any;
    files: any;
}

export interface Session {
    clear: Function;
    setItem: Function;
    getItem: Function;
}

export interface History {
    push: Function;
}

export interface Location {
    pathname: string;
    search: string;
}

export interface HeaderComponent {
    title?: string;
    headerBarTop?: boolean;
}

export interface PainelComponent {
    haveBarTop?: boolean;
    color?: string;
}

export interface ClassworkMenu {
    id: number;
    session: string;
    route: string;
    icon: string;
    name: string;
    url: string;
}

export interface IconsComponent {
    color?: string;
    size?: number;
    name: string;
}

export interface CredentialsFormComponent {
    option: string;
    need_password: string;
    user: UserObject;
}

export interface DirectoryComponent {
    _url: string;
}

export interface DropzoneComponent {
    errors: string[],
    onFileUploaded: (file: File, template: {
        url: string,
        filename: string
    }, reset: boolean) => void;
    template: string;
    reset: boolean;
}

export interface ColorPickerComponent {
    onColorChange: (color: string, previewColor: string) => void;
}

export interface ActiveShapeInterface {
    cx: number;
    cy: number;
    midAngle: number; 
    innerRadius: number;
    outerRadius: number;
    startAngle: number;
    endAngle: number; 
    payload: {
        name: string;
        avatar: string;
        total: number;
        additions: number;
        deletions: number;
    }
    percent: number; 
}

export interface TooltipInterface {
    active: boolean;
    label: string;
    payload: {
        value: string
    }[];
}

export interface TreeItem {
    path: string;
    type: string;
    url: string;
}

export interface FileItem {
    filename: string;
    status: string;
    additions: number;
    deletions: number;
}

export interface ClassItem {
    name: string;
    description: string;
    key: string;
    image: string;
    avatar: string;
    color: string;
}

export interface ClassInfoItem {
    name: string;
    description: string;
    key: string;
    invite: string;
    teacher_id: number;
    image: string;
    color: string;
}

export interface ClassTeacherItem {
    git_id: number;
    id_auth: string;
    real_name: string;
    name: string;
    avatar: string;
}

export interface ClassMemberItem {
    avatar: string;
    git_id: number;
    name: string;
    real_name: string;
    urls: string[];
}

export interface TemplateItem {
    url: string, 
    filename: string
}