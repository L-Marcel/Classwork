import { celebrate, Joi } from 'celebrate';

class Class {
    index(){
        return celebrate({
            body: Joi.object().keys({
                key: Joi.string().required().min(8),
                name: Joi.string().required().min(3).max(28), 
                description: [Joi.string().optional(), Joi.allow(null)],
                teacher_id: Joi.number().required(),
                teacher_id_auth: Joi.string().required(),
                color: Joi.string().required(),
                haveImage: Joi.required()
            })
        },{
            abortEarly: false
        });
    }
    template(){
        return celebrate({
            body: Joi.object().keys({
                key: Joi.string().required().min(8),
                name: Joi.string().required().min(3).max(28), 
                description: [Joi.string().optional(), Joi.allow(null)],
                teacher_id: Joi.number().required(),
                teacher_id_auth: Joi.string().required(),
                color: Joi.string().required(),
                filename: Joi.string().required()
            })
        },{
            abortEarly: false
        });
    }
    update(){
        return celebrate({
            body: Joi.object().keys({
                name: Joi.string().required().min(3).max(28), 
                escription: [Joi.string().optional(), Joi.allow(null)],
            }),

            query: Joi.object().keys({
                key: Joi.string().required()
            })
        },{
            abortEarly: false
        });
    }
    delete(){
        return celebrate({
            query: Joi.object().keys({
                key: Joi.string().required(),
                teacher_id: Joi.number().required()
            })
        },{
            abortEarly: false
        });
    }
    addMember(){
        return celebrate({
            body: Joi.object().keys({
                git_id: Joi.number().required(), 
                id_auth: Joi.string().required(), 
                invite: Joi.string().required()
            })
        }, {
            abortEarly: false
        });
    }
    removeMember(){
        return celebrate({
            body: Joi.object().keys({
                git_id: Joi.number().required(), 
                key: Joi.string().required(), 
            })
        }, {
            abortEarly: false
        });
    }
}

export default Class;