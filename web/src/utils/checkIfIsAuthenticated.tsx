import { Session, History, Location } from '../interfaces/global';

export default async function checkIfIsAuthenticated(session: Session, history: History, location: Location){
    var user = sessionStorage.getItem('user');
    if(user != null){
        if(location.pathname === "/"){
            history.push('/profile');
        }
    }else{
        if(location.pathname !== "/"){
            session.clear();
            history.push('/');
        }
    }
} 