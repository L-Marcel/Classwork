import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import HeaderAuth from '../../../components/headerAuth';
import Painel from '../../../components/painel';

import api from '../../../services/api';
import { ClassInfoItem, ClassTeacherItem, ClassMemberItem } from '../../../interfaces/global';
import { isNullOrUndefined } from 'util';

const ClassroomProfile = () => {
    const { key } = useParams();
    const [classInfo, setClassInfo] = useState<ClassInfoItem>();
    const [classTeacher, setClassTeacher] = useState<ClassTeacherItem>();
    const [classPainelInfo, setClassPainelInfo] = useState<string>('members');
    const [classMembers, setClassMembers] = useState<ClassMemberItem[]>();


    useEffect(() => {
        async function getClassInfo(){
            var _data;
            var _teacherData;
            await api.get(`/classes?key=${key}`, {
                headers: {
                    auth: process.env.REACT_APP_DB_IDENTITY
                }
            }).then(async function(res){
                _data = res.data as ClassInfoItem;
                await api.get(`/users?id=${_data.teacher_id}`, {
                    headers: {
                        auth: process.env.REACT_APP_DB_IDENTITY
                    }
                }).then(function(res){
                    _teacherData = {
                        git_id: res.data.git_id,
                        id_auth: res.data.id_auth,
                        real_name: res.data.real_name,
                        name: res.data.name,
                        avatar: res.data.avatar
                    };
                }).catch(function(err){
                    console.log(err);
                });
            }).catch(function(err){
                console.log(err);
            });

            setClassTeacher(_teacherData);
            setClassInfo(_data);
        }

        getClassInfo();
    }, [key]);

    useEffect(() => {
        async function getClassMembers(){
            var _data;
            await api.get(`/class/members?key=${key}`, {
                headers: {
                    auth: process.env.REACT_APP_DB_IDENTITY
                }
            }).then(function(res){
                console.log(res.data);
                _data = res.data;
            }).catch(function(err){
                console.log(err);
            });
            setClassMembers(_data);
        }
        getClassMembers();
        switch(classPainelInfo){
            case('members'): getClassMembers(); break;
        }
    }, [key, classPainelInfo]);

    function handleCopyInvite(e: React.MouseEvent<HTMLHeadingElement, MouseEvent>) {
        if(!isNullOrUndefined(classInfo)){
            navigator.clipboard.writeText(classInfo.invite);
        }
    }

    return(
        <div>
            <HeaderAuth title="" headerBarTop={false}/>
            <Painel haveBarTop={false} color={classInfo?.color}/>
            <div className="div-global-page with-scroll header-is-auth" style={{ height: '100vh' }}>
                <div style={{ display: "flex"}}>
                { classInfo && classTeacher && <>
                <div style={{ display: 'flex', flexDirection: 'column', width: '45vw'}}>
                    <div className="class-profile shadow-theme-lit-current-color" style={{ color: classInfo.color }}>
                        <div>
                            <div className='class-profile-banner'
                            style={{ backgroundImage: `url(${process.env.REACT_APP_URL_BACK}/classes/uploads/${classInfo.image})`}}>
                            </div>
                        </div>
                        <div className='class-profile-banner-info'>
                            <img src={classTeacher.avatar} alt="professor"/>
                            <div>
                                <h1>{classInfo.name}</h1>
                                <h3>{classTeacher.real_name} | {classTeacher.name}</h3>
                                <div>
                                    <h4 style={{ backgroundColor: classInfo.color }}>{classInfo.invite}</h4>
                                    <h4 style={{ backgroundColor: classInfo.color }} onClick={handleCopyInvite}>Copiar</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    {classInfo.description && <div className='class-profile-banner-info-description theme-color-scrollbar shadow-theme-lit-current-color' style={{ color: classInfo.color }}>
                        <p>{classInfo.description}</p>
                    </div>}
                </div>

                <div className='class-profile-area' style={{ color: classInfo.color }}>
                    <ul>
                        <li>
                            <button>
                                Alunos
                            </button>
                        </li>
                        <li>
                            <button>
                                Equipes
                            </button>
                        </li>
                        <li>
                            <button>
                                Avisos
                            </button>
                        </li>
                    </ul>
                    <div style={{ color: classInfo.color }}>
                        {classMembers? classMembers.map(function(item, index){
                            return(<div key={index} className="classses-info-block-item shadow-theme-lit-current-color">
                            <div>
                                <img src={item.avatar} alt=""/>
                            </div>
                            <div className="block-item-text">
                                <h1>{item.real_name} | {item.name}</h1>
                                <h2>Static Team Name</h2>
                            </div>
                        </div>);
                        }):
                        <div className="classses-info-block-item shadow-theme-lit-current-color">
                            <div className="block-item-text">
                                <h1>Nenhum aluno cadastrado</h1>
                            </div>
                        </div>}
                    </div>
                </div></>}
            </div>
        </div>
    </div>);
}

export default ClassroomProfile;
