import { Request, Response } from 'express';
import axios from 'axios';
import generateHex from '../utils/generateHex';
import dateReturn from '../utils/dateReturn';

class GitsControlles {
    async git(req: Request, res: Response) {
        const GITHUB_AUTH_TOKEN_URL = 'https://github.com/login/oauth/access_token';
        const GITHUB_USER_URL = 'https://api.github.com/user';
        const END_URL = process.env.REACT_APP_URL_FRONT;
        var client_id = process.env.REACT_APP_GH_BASIC_CLIENT_ID;
        var secret_id = process.env.REACT_APP_GH_BASIC_SECRET_ID;

        if(process.env.NODE_ENV === "development"){
            client_id = process.env.REACT_APP_GH_BASIC_CLIENT_ID_DEV;
            secret_id = process.env.REACT_APP_GH_BASIC_SECRET_ID_DEV;
        }

        const STATE_APP = req.query.state;
        const CODE = req.query.code;
        const SCOPE = "repo, user";

        var isNew = false;
        var token = '';
        var user = {
            id: -1,
            repos_url: "",
            login: "",
            name: "",
            avatar_url: "",
            url: ""
        };
        var need_password = true;
        var error = false;


        //Pegar o token do Git
        await axios({
                url: GITHUB_AUTH_TOKEN_URL,
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'X-OAuth-Scopes': SCOPE,
                },
                data: {
                    client_id: client_id,
                    client_secret: secret_id,
                    state: STATE_APP,
                    code: CODE,
                }
        }).then(function(response){
            token = response.data.access_token;
        }).catch(function(err){
            error = true;
            console.log(dateReturn() + 'Error in get code from token');
        });

        //Pedir dados do usuário para o git
        await axios({   
                method: 'get',
                url: GITHUB_USER_URL,
                headers: {
                    'Authorization': 'token ' + token,
                    'X-OAuth-Scopes': SCOPE,
                },
        }).then(function(response){
            user = response.data;
        }).catch(function(err){
            error = true;
            console.log(dateReturn() + 'Error in send token');
        });


        //Checar se existe algum usuário no banco de dados com o msm git_id
        await axios({
            method: 'get',
            url: process.env.REACT_APP_URL_BACK + '/users?git_id=' + user.id,
            headers: {
                'auth': process.env.REACT_APP_DB_IDENTITY,
            },
        }).then(async function(response){
            need_password = false;
            user = response.data; 
        }).catch(async function(){
            console.log(dateReturn() + 'Checking user');

            //Criar se não existir
            if(user.id != null){
                var git_id = user.id;
                var name = user.login;
                var real_name = String(user.name);
                var type = "User";
                var avatar = user.avatar_url;
                var email =  user.login + "@classwork.com";
                var password = "User@" + generateHex();
                var id_auth = generateHex();
                var urls = [ 
                    user.url, 
                    user.repos_url 
                ];
                var classes = [] as string[];
                var teams = [] as any[];

                await axios({
                    method: 'post',
                    url: process.env.REACT_APP_URL_BACK + '/user/create',
                    data: {
                        id_auth,
                        git_id, 
                        name, 
                        real_name, 
                        type, 
                        avatar, 
                        password,
                        email,
                        urls,
                        classes,
                        teams
                    },
                    headers: {
                        'auth': process.env.REACT_APP_DB_IDENTITY,
                    },
                }).then(function(response){
                    need_password = true;
                    user = response.data; 
                    isNew = true;
                }).catch(function(err){
                    error = true;
                    console.log(dateReturn() + 'Error in create user');
                });
            }
        });
        
        if(isNew){
            return res.status(200).redirect(END_URL+`/profile/register?token=${token}&error=${error}&need_password=${need_password}&user=${JSON.stringify(user)}`);
        }else{
            return res.status(200).redirect(END_URL+`/profile?token=${token}&user=${JSON.stringify(user)}`);
        }
    }
    
    async getGitRepos(req: Request, res: Response) {
        var { token, page } = req.query;
        var user = null as any;
        await axios({   
                method: 'get',
                url: 'https://api.github.com/user',
                headers: {
                    'Authorization': 'token ' + token,
                },
        }).then(function(response){
            user = response.data;
        }).catch(function(err){
            console.log(dateReturn() + 'Error in get repos');
            res.status(400);
        });
        var repos = [] as any[];
        await axios(
            {   
                method: 'get',
                url: `https://api.github.com/users/${user.login}/repos?per_page=33&page=${page}`,
                headers: {
                    'Authorization': 'token ' + token
                },
                data: {
                    type: 'all',
                },
            }
        ).then(function(response){
            for(var i in response.data){
                repos.push({
                    id: response.data[i].id,
                    name: response.data[i].name,
                    description: response.data[i].description,
                    language: response.data[i].language,
                    private: response.data[i].private,
                    commits_url: response.data[i].commits_url,
                    size: response.data[i].size
                });
            }
        }).catch(function(err){
            console.log(dateReturn() + 'Error in get repos');
            res.status(400);
        });
        res.status(200).json(repos);
    }
    
    async raw(req: Request, res: Response) {
        const raw_url = String(req.query.raw_url);
        const TOKEN = String(req.query.token);
        if(raw_url != null){
            var raw;
            await axios.get(raw_url, {
                headers: {
                    'Authorization': 'token ' + TOKEN
                },
            }).then(function(response){
                raw = response.data;
            }).catch(function(){});
            
            return res.status(200).json(raw);
        }else{
            console.log(dateReturn() + `Raw url is not defined!`);
            return res.status(404).json({
                "message": "Operação inválida",
                "origin": "Database",
            });
        }
    }
}

export default GitsControlles;