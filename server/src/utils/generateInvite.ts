import crypto from 'crypto';

export default function generateUniqueInvite(id_auth: string, key: string){
    return key.toString().substr(2,4) + crypto.randomBytes(2).toString('hex') + id_auth.toString().substr(0,2);
}