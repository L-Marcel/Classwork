import multer from 'multer';
import parth from 'path';

export default {
    storage: multer.diskStorage({
        destination: parth.resolve(__dirname, 'uploads'),
        filename(req, file, callback) {
            const filename = `${req.body.key}.${file.mimetype.split("\/")[1]}`;
            callback(null, filename);
        }
    }),
}