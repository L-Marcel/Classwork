import React from 'react';
import { FaGithubAlt } from 'react-icons/fa';
import { useLocation, useHistory } from 'react-router-dom';

import Header from '../../components/header';
import CredentialsForm from '../../components/credentialsForm';

import queryToStorage from '../../utils/queryToStorage';
import checkIfIsAuthenticated from '../../utils/checkIfIsAuthenticated';
import { NotNullUser } from '../../interfaces/global';

const Login = () => {
    const history = useHistory();
    const location = useLocation();
    const GITHUB_AUTH_URL = 'https://github.com/login/oauth/authorize';
    const STATE_APP = process.env.REACT_APP_STATE;
    const CLIENT_ID = (process.env.NODE_ENV === "development")? 
    process.env.REACT_APP_GH_BASIC_CLIENT_ID_DEV : 
    process.env.REACT_APP_GH_BASIC_CLIENT_ID;
    console.log(process.env.REACT_APP_GH_BASIC_CLIENT_ID_DEV, process.env.REACT_APP_GH_BASIC_CLIENT_ID_DEV, process.env.NODE_ENV)
    const URL = `${GITHUB_AUTH_URL}?client_id=${CLIENT_ID}&state=${STATE_APP}&scope=user%20repo`;

    queryToStorage(sessionStorage, history, location, 'user', 'error');

    if(sessionStorage.getItem('error') === "true"){
        sessionStorage.removeItem('user');
        alert("Dados incorretos!");
        sessionStorage.removeItem('error');
    }

    checkIfIsAuthenticated(sessionStorage, history, location);

    return( 
        <div>
            <Header title="Classwork"/>
            <div className="div-global-page-dooble">
                <div className="div-global-page">
                    <div className="div-introduction-title div-introduction-title-normal">
                        <h1>Já possui conta no Github?</h1>
                        <h2>O que está esperando? Entre com uma agora mesmo e procure sua turma!</h2>
                    </div>
                    <div className="div-alt">
                        <div className="alt-login-container git shadow">
                            <a href={URL}>
                                <FaGithubAlt size={35}/>
                                <h3>Entrar com Github</h3>
                            </a>
                        </div>
                    </div>
                    <CredentialsForm need_password="false" option="login" user={NotNullUser}/>
                </div>
            </div>
        </div>
    );
}

export default Login;