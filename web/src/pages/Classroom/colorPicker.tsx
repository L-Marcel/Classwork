import React, { useState } from 'react';
import { TwitterPicker, SliderPicker, ColorResult } from 'react-color';
import { ColorPickerComponent } from '../../interfaces/global'; 

const ColoPicker: React.FC<ColorPickerComponent> = ({ onColorChange }) => {
    const [color, setColor] = useState('#4682b4');
    const [previewColor, setPreviewColor] = useState(color);

    function handleHoverColor(houverColor: ColorResult, e: MouseEvent) {
        e.preventDefault();
        setPreviewColor(houverColor.hex);
        onColorChange(color, houverColor.hex);
    }

    function handleColorSlider(_color: ColorResult) {
        setPreviewColor(_color.hex);
        setColor(_color.hex);
        onColorChange(_color.hex, _color.hex);
    }

    function handleSetColor(color: ColorResult){
        setColor(color.hex);
        onColorChange(color.hex, previewColor);
    }

    return(
        <div style={{ marginTop: '10px'}}>
            <TwitterPicker colors={[
            '#4682B4',
            '#BF4063',
            '#BF4099',
            '#9A40BF',
            '#5540BF',
            '#4046BF', 
            '#40BFB2', 
            '#47BF40', 
            '#7DBF40', 
            '#B3BF40', 
            '#BF9940', 
            '#BF6340', 
            '#BF4040',
            ]} color={color} triangle="hide" width="100%" onSwatchHover={handleHoverColor} onChange={handleSetColor} onChangeComplete={handleSetColor}/>
            <div className="slider-picker-color">
            <SliderPicker color={color} onChange={handleColorSlider} onChangeComplete={handleSetColor}/>
            </div>
        </div>
    );
}

export default ColoPicker;