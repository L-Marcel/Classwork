import express from 'express';
import multer from 'multer';
import configMulter from '../multer';

const upload = multer(configMulter);

import GitsController from './controllers/gits';
import UsersController from './controllers/users';
import ClassesController from './controllers/classes';

import ExternalSecurity from './security/external';

const routes = express.Router();

const gitController = new GitsController();
const userController = new UsersController();
const classController = new ClassesController();

const externalSecurity = new ExternalSecurity();

//User
routes.post('/user/create', externalSecurity.user.index(), userController.create);
routes.post('/user/update', externalSecurity.user.update(), userController.update);
routes.post('/user/updateCredentials', externalSecurity.user.credentials(), userController.updateCredentials);
routes.post('/login', userController.login);
routes.delete('/user/delete', externalSecurity.user.delete(), userController.delete);
routes.get('/users', userController.list);

//Class
routes.post('/class/create', upload.single('image'), externalSecurity.class.index(), classController.create);
routes.post('/class/create/template', externalSecurity.class.template(), classController.createWithTemplate);
routes.post('/class/update', externalSecurity.class.update(), classController.update);
routes.post('/class/members/add', externalSecurity.class.addMember(), classController.addMember);
routes.post('/class/members/remove', externalSecurity.class.removeMember(), classController.removeMember);
routes.post('/class/teams/create', classController.createTeams);
routes.delete('/class/delete', externalSecurity.class.delete(), classController.delete);
routes.get('/class/members', classController.getMembers);
routes.get('/class/teams', classController.getTeams)
routes.get('/classes', classController.list);

//Git
routes.get('/git', externalSecurity.git.index(), gitController.git);
routes.get('/git/user/repos', gitController.getGitRepos);
routes.get('/raw', gitController.raw);

export default routes;