import { NotNullUser } from '../interfaces/global';
import { isNullOrUndefined } from 'util';

export default function returnNotNullSession(sessionName: string, defaultValue: string){
    var _session = sessionStorage.getItem(sessionName);
    if(isNullOrUndefined(_session)){
        switch(sessionName){
            case('user'): return JSON.stringify(NotNullUser);
            case('action'): return "";
            default: return defaultValue;
        }

    }
    return _session;
}
