import { Request, Response } from 'express';
import connection from '../database/connection';
import InternalSecurity from '../security/internal';
import dateReturn from '../utils/dateReturn';
import generateInvite from '../utils/generateInvite';
import jsonParseCheck from '../utils/jsonParseCheck';
import { isNullOrUndefined } from 'util';

const internalSecurity = new InternalSecurity();

class ClassesControllers {
    async create(req: Request, res: Response) {
        const { name, description, teacher_id, teacher_id_auth, key, color } = req.body;
        const invite = generateInvite(teacher_id, key);
        console.log(req.body);
        var thx = await connection.transaction();
        const { auth } = req.headers;

        if(internalSecurity.checkIsAuthorized(String(auth))){
            console.log(dateReturn() + `New class created!`);
            await thx('classes').insert({
                image: req.file.filename,
                color,
                key,
                name,
                description,
                invite,
                teacher_id,
                teacher_id_auth
            });

            var user = await thx('users').where('id_auth', teacher_id_auth).first();
            if(!user){
                await thx.rollback();
                console.log(dateReturn() + `User [${teacher_id}] is not defined!`);
                return res.status(404).json({
                    "message": "Operação inválida",
                    "origin": "Database",
                });
            }

            var classes = (isNullOrUndefined(user.classes) || user.classes.length <= 0)? []:jsonParseCheck(user.classes); 
            classes.push(key);

            await thx('users').where('id_auth', teacher_id_auth).update({
                classes: JSON.stringify(classes)
            });
            user = await thx('users').where('id_auth', teacher_id_auth).first();
            await thx.commit();

            user.classes = jsonParseCheck(user.classes);
            user.urls = jsonParseCheck(user.urls);
            user.teams = jsonParseCheck(user.teams);
            return res.status(201).json(user);
        }else{
            await thx.commit();
            console.log(dateReturn() + `Unauthorized request has been blocked!`);
            return res.status(203).json({
                    "message": "Request não autorizado!",
                    "origin": "Internal security",
            });
        }
    }
    async createWithTemplate(req: Request, res: Response) {
        const { name, description, teacher_id, teacher_id_auth, key, filename, color } = req.body;
        const invite = generateInvite(teacher_id, key);

        var thx = await connection.transaction();
        const { auth } = req.headers;

        if(internalSecurity.checkIsAuthorized(String(auth))){
            console.log(dateReturn() + `New class created!`);
            await thx('classes').insert({
                image: filename,
                color,
                key,
                name,
                description,
                invite,
                teacher_id,
                teacher_id_auth
            });

            var user = await thx('users').where('id_auth', teacher_id_auth).first();
            if(!user){
                await thx.rollback();
                console.log(dateReturn() + `User [${teacher_id}] is not defined!`);
                return res.status(404).json({
                    "message": "Operação inválida",
                    "origin": "Database",
                });
            }
            var classes = (isNullOrUndefined(user.classes) || user.classes.length <= 0)? []:jsonParseCheck(user.classes);
            classes.push(key);

            await thx('users').where('id_auth', teacher_id_auth).update({
                classes: JSON.stringify(classes)
            });
            user = await thx('users').where('id_auth', teacher_id_auth).first();
            await thx.commit();

            user.classes = jsonParseCheck(user.classes);
            user.urls = jsonParseCheck(user.urls);
            user.teams = jsonParseCheck(user.teams);
            return res.status(201).json(user);
        }else{
            await thx.commit();
            console.log(dateReturn() + `Unauthorized request has been blocked!`);
            return res.status(203).json({
                    "message": "Request não autorizado!",
                    "origin": "Internal security",
            });
        }
    }
    async list(req: Request, res: Response) {
        var queryKey = req.query.key;

        var classes = null;
        const { auth } = req.headers;

        if(internalSecurity.checkIsAuthorized(String(auth))){
            if(queryKey != null){
                classes = await connection('classes').select(['color', 'name','description', 'key', 'invite', 'image', 'teacher_id']).where("key", String(queryKey)).first();

                if(classes == null){
                    console.log(dateReturn() + `Class [${queryKey}] is not defined!`);
                    return res.status(404).json({
                        "message": "Operação inválida",
                        "origin": "Database",
                    });
                }
                console.log(dateReturn() + `Class [${queryKey}] listed!`);
            }else{
                classes = await connection('classes').select(['color', 'name','description', 'key', 'invite', 'image', 'teacher_id']);
            }
            console.log(dateReturn() + `Classes listed!`);
            return res.status(202).json(classes);
        }else{
            console.log(dateReturn() + `Unauthorized request has been blocked!`);
            return res.status(203).json({
                    "message": "Request não autorizado!",
                    "origin": "Internal security",
            });
        }
    }
    async delete(req: Request, res: Response) {
        const queryKey = req.query.key;
        const queryTeacherId = req.query.teacher_id;

        var classes = null;
        const { auth } = req.headers;

        if(internalSecurity.checkIsAuthorized(String(auth))){
            if(queryKey != null){
                classes = await connection('classes').select(['name','description', 'key', 'invite']).where("key", String(queryKey)).andWhere("teacher_id", String(queryTeacherId)).first();
                if(classes == null){
                    console.log(dateReturn() + `Class [${queryKey},${queryTeacherId}] is not defined!`);
                    return res.status(404).json({
                        "message": "Operação inválida",
                        "origin": "Database",
                    });
                }
                await connection('classes').select(['name','description', 'key', 'invite']).where("key", String(queryKey)).andWhere("teacher_id", String(queryTeacherId)).first().delete();
                console.log(dateReturn() + `Class [${queryKey},${queryTeacherId}] deleted!`);
                return res.status(200).json(classes);
            }else{
                console.log(dateReturn() + `Class [${queryKey}] is not defined!`);
                return res.status(404).json({
                    "message": "Operação inválida",
                    "origin": "Database",
                });
            }
        }else{
            console.log(dateReturn() + `Unauthorized request has been blocked!`);
            return res.status(203).json({
                    "message": "Request não autorizado!",
                    "origin": "Internal security",
            });
        }
    }
    async update(req: Request, res: Response) {
        const { name, description } = req.body;

        const queryKey = req.query.key;

        var classes = null;
        const { auth } = req.headers;

        if(internalSecurity.checkIsAuthorized(String(auth))){
            if(queryKey != null){
                classes = await connection('classes').select(['name','description', 'key', 'invite']).where("key", String(queryKey)).first();
                if(classes == null){
                    console.log(dateReturn() + `Class [${queryKey}] is not defined!`);
                    return res.status(404).json({
                        "message": "Operação inválida",
                        "origin": "Database",
                    });
                }
                await connection('classes').select(['name','description', 'key', 'invite']).where("key", String(queryKey)).update({
                    name,
                    description
                });

                console.log(dateReturn() + `Class [${queryKey}] updated!`);
                return res.status(200).json(classes);
            }else{
                console.log(dateReturn() + `Class [${queryKey}] is not defined!`);
                return res.status(404).json({
                    "message": "Operação inválida",
                    "origin": "Database",
                });
            }
        }else{
            console.log(dateReturn() + `Unauthorized request has been blocked!`);
            return res.status(203).json({
                    "message": "Request não autorizado!",
                    "origin": "Internal security",
            });
        }
    }
    async addMember(req: Request, res: Response) {
        var { git_id, id_auth, invite } = req.body;

        var thx = await connection.transaction();
        const { auth } = req.headers;

        if(internalSecurity.checkIsAuthorized(String(auth))){
            if(invite != null){
                var _class = await thx('classes').where("invite", invite).first();
                
                if(!_class){
                    await thx.rollback();
                    console.log(dateReturn() + `Class [${_class}] is not defined!`);
                    return res.status(404).json({
                        "message": "Operação inválida",
                        "origin": "Database",
                    });
                }else if(_class.teacher_id_auth == id_auth){
                    await thx.rollback();
                    console.log(dateReturn() + `Member [${git_id}] is the teacher in Class [${_class.key}]!`);
                    return res.status(404).json({
                        "message": "Operação inválida",
                        "origin": "Database",
                    });
                }

                var user = await thx('users').where('git_id', git_id).first();

                var classes = isNullOrUndefined(user.classes)? []:jsonParseCheck(user.classes);

                if(_class.members === null){
                    let first = [git_id];
                    _class.members = JSON.stringify(first);
                }else{
                    _class.members = jsonParseCheck(_class.members);
                    for(var m in _class.members){
                        if(_class.members[m] === git_id){
                            await thx.rollback();
                            console.log(dateReturn() + `Member [${git_id}] is already registered in Class [${_class.key}]!`);
                            return res.status(404).json({
                                "message": "Operação inválida",
                                "origin": "Database",
                            });
                        }
                    }
                    console.log(_class.members);
                    _class.members.push(git_id);
                    _class.members = JSON.stringify(_class.members);
                }
                classes.push(_class.key);

                var members = _class.members;
                await thx('classes').where("key", _class.key).update({
                    members
                });
                await thx('users').where('git_id', git_id).update({
                    classes: JSON.stringify(classes)
                });
                user = await thx('users').where('git_id', git_id).first();
                await thx.commit();

                console.log(dateReturn() + `Class [${_class.key}] have a new Member [${git_id}]!`);
                user.classes = jsonParseCheck(user.classes);
                user.urls = jsonParseCheck(user.urls);
                user.teams = jsonParseCheck(user.teams);
                return res.status(200).json(user);
            }else{
                await thx.rollback();
                console.log(dateReturn() + `Class [Undefined] is not defined!`);
                return res.status(404).json({
                    "message": "Operação inválida",
                    "origin": "Database",
                });
            }
        }else{
            await thx.rollback();
            console.log(dateReturn() + `Unauthorized request has been blocked!`);
            return res.status(203).json({
                    "message": "Request não autorizado!",
                    "origin": "Internal security",
            });
        }
    }
    async getMembers(req: Request, res: Response) {
        var queryKey = req.query.key;
        
        const { auth } = req.headers;
        console.log(queryKey);

        if(internalSecurity.checkIsAuthorized(String(auth))){
            if(queryKey != null){
                var classes = await connection('classes').select(['members']).where("key", String(queryKey)).first();
                if(classes == null){
                    console.log(dateReturn() + `Class [${queryKey}] is not defined!`);
                    return res.status(404).json({
                        "message": "Operação inválida",
                        "origin": "Database",
                    });
                }
                console.log(classes.members);
                classes.members = jsonParseCheck(classes.members);
                console.log(classes.members);
                console.log(classes);
                console.log(dateReturn() + `Class members [${queryKey}] listed!`);
            }else{
                console.log(dateReturn() + `Class [${queryKey}] is not defined!`);
                return res.status(404).json({
                    "message": "Operação inválida",
                    "origin": "Database",
                });
            }
            var _members = {
                data: [] as any
            };
            for(var m in classes.members){
                    var member = await connection('users').where('git_id', classes.members[m]).first(); 
                    _members.data[m] = {
                        git_id: member.git_id,
                        real_name: member.real_name,
                        name: member.name,
                        avatar: member.avatar,
                        urls: jsonParseCheck(member.urls)
                    }
            }
            return res.status(202).json(_members.data);
        }else{
            console.log(dateReturn() + `Unauthorized request has been blocked!`);
            return res.status(203).json({
                    "message": "Request não autorizado!",
                    "origin": "Internal security",
            });
        }
    }
    async removeMember(req: Request, res: Response) {
        const { key, git_id } = req.body;

        var thx = await connection.transaction();
        const { auth } = req.headers;

        if(internalSecurity.checkIsAuthorized(String(auth))){
            var user = await thx('users').select('*').where('git_id', git_id).first();
            var _class = await thx('classes').select('*').where('key', key).first();

            var classes = user.classes as String[];
            var members = _class.members as number[];

            if(!classes || !members){
                await thx.rollback();
                console.log(dateReturn() + `Class or Member is not defined!`);
                return res.status(404).json({
                    "message": "Operação inválida",
                    "origin": "Database",
                });
            }

            var _members: number[] = [];
            members.map(function(member){
                if(member !== git_id){
                    _members.push(member);
                }
            });   
            members = _members;

            var _classes: string[] = [];
            classes.map(function(_key){
                if(_key !== key){
                    _classes.push(key);
                }
            });
            classes = _classes;

            await thx('classes').where("key", key).update({
                members
            });

            await thx('users').where('git_id', git_id).update({
                classes: JSON.stringify(classes)
            });

            await thx.commit();

            return res.status(200).json(members);
        }else{
            await thx.rollback();
            console.log(dateReturn() + `Unauthorized request has been blocked!`);
            return res.status(203).json({
                    "message": "Request não autorizado!",
                    "origin": "Internal security",
            });
        }
    }
    async createTeams(req: Request, res: Response) {
        const { key, members, goals, repos, name } = req.body;

        var thx = await connection.transaction();
        const { auth } = req.headers;

        if(internalSecurity.checkIsAuthorized(String(auth))){
            const classes = await thx('classes').where('key', key).first();
            if(!classes || classes.length < 1){
                await thx.rollback();
                console.log(dateReturn() + `Class [${classes}] is not defined!`);
                return res.status(404).json({
                    "message": "Operação inválida",
                    "origin": "Database",
                });
            }
            console.log(members);
            const id = await thx('teams').insert({
                name,
                members: JSON.stringify(members),
                goals,
                repos
            });

            await thx('classes_teams').insert({
                class_id: classes.id,
                team_id: id[0]
            });

            await thx.commit();

            console.log(dateReturn() + `Team in Class [${key}] as been created!`);
            return res.status(200).json({
                members,
                goals,
                name,
                repos
            });
        }else{
            await thx.rollback();
            console.log(dateReturn() + `Unauthorized request has been blocked!`);
            return res.status(203).json({
                    "message": "Request não autorizado!",
                    "origin": "Internal security",
            });
        }
    }
    async getTeams(req: Request, res: Response) {
        var key = req.query.key;
        
        var thx = await connection.transaction();
        const { auth } = req.headers;

        if(internalSecurity.checkIsAuthorized(String(auth))){
            if(key != null){
                var classes = await thx('classes').select(['id','key']).where("key", String(key)).first();
                if(classes == null){
                    await thx.rollback();
                    console.log(dateReturn() + `Class [${key}] is not defined!`);
                    return res.status(404).json({
                        "message": "Operação inválida",
                        "origin": "Database",
                    });
                }
            }else{
                await thx.rollback();
                console.log(dateReturn() + `Class [${key}] is not defined!`);
                return res.status(404).json({
                    "message": "Operação inválida",
                    "origin": "Database",
                });
            }

            const teams = await thx('teams').join('classes_teams', 'teams.id', '=', 'classes_teams.team_id')
            .whereIn('classes_teams.class_id', [String(classes.id)])
            .distinct()
            .select('teams.*');

            await thx.commit();

            console.log(dateReturn() + `Class teams [${key}] listed!`);
            return res.status(202).json(teams);
        }else{
            await thx.rollback();
            console.log(dateReturn() + `Unauthorized request has been blocked!`);
            return res.status(203).json({
                    "message": "Request não autorizado!",
                    "origin": "Internal security",
            });
        }
    }
    async destroyTeams(req: Request, res: Response) {

    }
    async addWarning(req: Request, res: Response) {

    }
    async getWarnings(req: Request, res: Response) {

    }
    async removeWarning(req: Request, res: Response) {

    }
}

export default ClassesControllers;