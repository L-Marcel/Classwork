import React, { useState, useEffect, FormEvent, MouseEvent, ChangeEvent } from 'react';
import { useHistory, useLocation } from "react-router-dom";
import { Link } from 'react-router-dom';

import { ClassItem } from '../../interfaces/global';

import HeaderAuth from '../../components/headerAuth';

import api from '../../services/api';

import checkIfIsAuthenticated from '../../utils/checkIfIsAuthenticated';
import returnNotNullSession from '../../utils/returnNotNullSession';
import jsonParseCheck from '../../utils/jsonParseCheck';

const Classroom = () => {
    const history = useHistory();
    const location = useLocation();

    const [classes, setClasses] = useState<ClassItem[]>([]);
    const [classesKey, setClassesKey] = useState<string[]>([]);
    const [invite, setInvite] = useState<string>('');

    checkIfIsAuthenticated(sessionStorage, history, location);

    useEffect(() => {
        setClassesKey(JSON.parse(returnNotNullSession('user', '')).classes);
    }, []);

    useEffect(() => {
        async function loadClasses(){
            var allClasses: ClassItem[] = [];
            for(var t in classesKey){
                await api(`classes?key=${classesKey[t]}`, {
                    headers: {
                        auth: process.env.REACT_APP_DB_IDENTITY
                    } 
                }).then(async(res) => {
                    var teacher = await api(`users?id=${res.data.teacher_id}`, {
                        headers: {
                            auth: process.env.REACT_APP_DB_IDENTITY
                        }
                    });

                    const data = {
                        name: res.data.name,
                        description: res.data.description,
                        key: res.data.key,
                        image: res.data.image,
                        avatar: teacher.data.avatar,
                        color: res.data.color
                    }
                    allClasses.push(data);  
                }).catch();
            }
            setClasses(allClasses);
        }

        loadClasses();
    }, [classesKey]);

    function handleFormPrevent(e: FormEvent){
        e.preventDefault();
    }

    function handleChangeInvite(e: ChangeEvent<HTMLInputElement>){
        const invite = e.currentTarget.value;
        if(invite.length <= 9){
            setInvite(e.currentTarget.value);
        }
    }

    async function handleCheckInvite(e: MouseEvent<HTMLButtonElement>){
        const user = JSON.parse(returnNotNullSession('user',''));
        await api.post('/class/members/add', {
            invite,
            git_id: user.git_id,
            id_auth: user.id_auth
        }, {
            headers: {
                auth: process.env.REACT_APP_DB_IDENTITY
            }
        }).then(function(res){
            sessionStorage.setItem('user', JSON.stringify(res.data));
            const _classes = JSON.parse(returnNotNullSession('user', '')).classes;
            setClassesKey(_classes);
            navigateToClassProfile(_classes[_classes.length - 1]);
        }).catch(function(err){
            console.log(err.response.data);
        });
    }

    function navigateToClassProfile(key: string) {
        history.push(`/class/${key}`);
    }

    const ListClasses = classes.map(function(item, index){
        var name = item.name;
        var description = item.description;

        if(description === null || description === ""){
            description = "Sem descrição";
        }else if(description.length >= 128){
            description = description.substr(0, 125) + "...";
        }

        if(name.length > 24){
            name = name.substr(0, 24) + '...';
        }

        return (<li key={index}><button style={{ color: item.color }} className="shadow-theme-lit-current-color class-info-display" 
        onClick={() => {navigateToClassProfile(item.key)}}>
                <div className="box-list-div-image">
                    <div className="box-list-div-class-image remove-color-transition" style={{ backgroundImage: `url(${process.env.REACT_APP_URL_BACK}/classes/uploads/${item.image})`}}>
                        <img src={item.avatar} alt="professor"/>
                    </div>
                </div>
                <div className="class-info-content">
                    <h3>{name}</h3>
                    <h4>{description}</h4>
                </div>
            </button>
        </li>);
    });

    return(
        <div>
            <HeaderAuth title="Error"/>
            <div className="div-global-page with-scroll header-is-auth">
                <div className="div-introduction-title-normal">
                    <h1>Turmas.</h1>
                    <h2>Lista de todas as turmas em que participa ou administra: </h2>
                </div>
                <div className="container-box-item-create">
                    <form onSubmit={handleFormPrevent}>
                        <input id="invite" name="invite" value={invite} onChange={handleChangeInvite} placeholder="Código de convite"/>
                        <button onClick={handleCheckInvite}>Entrar</button>
                    </form>
                    <Link to="/class/create">
                        <h1>Criar</h1>
                    </Link>
                </div>
                <div className="container-box-list container-box-list-within-padding">
                <ul>
                    {classes && classes.length > 0? ListClasses:null}
                </ul>
            </div>
            </div>
        </div>
        );
}

export default Classroom;