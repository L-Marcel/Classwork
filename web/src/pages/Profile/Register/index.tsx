import React from 'react';

import { useLocation, useHistory } from 'react-router-dom';

import Header from '../../../components/header';
import CredentialsForm from '../../../components/credentialsForm';

import queryToStorage from '../../../utils/queryToStorage';
import checkIfIsAuthenticated from '../../../utils/checkIfIsAuthenticated';
import returnNotNullSession from '../../../utils/returnNotNullSession';
import { isNullOrUndefined } from 'util';

const Register = () => {
    const history = useHistory();
    const location = useLocation();

    queryToStorage(sessionStorage, history, location, 'user', 'token', 'error', 'need_password');
    checkIfIsAuthenticated(sessionStorage, history, location);

    var need_password = sessionStorage.getItem('need_password');
    if(isNullOrUndefined(need_password)){
        need_password = "false";
    }

    var _user = returnNotNullSession('user', '');

    var error = sessionStorage.getItem('error');
    if(error === "true"){
        history.push('/error');
    }

    return( 
        <div>
            <Header/>
            <div className="div-global-page">
                <div className="div-introduction-title">
                    <h1>Atualize sua conta facilmente!</h1>
                    <h2>Informe um email e uma senha para sua conta Classwork</h2>
                </div>
                <div className="div-alt">
                    <CredentialsForm option="cadastro" need_password={need_password} user={JSON.parse(_user)}/>
                </div>
            </div>
        </div>
    );
}

export default Register;