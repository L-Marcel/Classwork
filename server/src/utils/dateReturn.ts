export default function dateReturn(){
    var dateConsole = new Date();

    var day = ``;
    if(dateConsole.getDay() < 10){
        day = `0${dateConsole.getDay()}`;
    }else{
        day = `${dateConsole.getDay()}`;
    }

    var month = ``;
    if(dateConsole.getMonth() < 10){
        month = `0${dateConsole.getMonth()}`;
    }else{
        month = `${dateConsole.getMonth()}`;
    }

    var hours = ``;
    if(dateConsole.getHours() < 10){
        hours = `0${dateConsole.getHours()}`;
    }else{
        hours = `${dateConsole.getHours()}`;
    }

    var minute = ``;
    if(dateConsole.getMinutes() < 10){
        minute = `0${dateConsole.getMinutes()}`;
    }else{
        minute = `${dateConsole.getMinutes()}`;
    }

    var dateReturn = `${day}/${month}/${dateConsole.getFullYear()} [${hours}:${minute}] - `;
    return dateReturn;
}